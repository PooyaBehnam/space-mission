#  Manage space missions travelling to Mars.
    I used Typescript and Dockerized Project and deployed it on linux base server. It's available on https://space-mission.pooyabehnam.ir 

    Third-party Package I used in this project:
    material-UI: MUI provides a simple, customizable, and accessible library of React components
    react-router-dom: for handling routes


## Run Project

### For Development

    I used create-react-app cli.
        1.yarn install
        2.yarn start
        it's available on port 3000

### For Production

    I Dockerized Project and deploy it on a Linux base server , It's available on https://space-mission.pooyabehnam.ir
    We have two container one container for webServer (nginx) and another for the app that made by Dockerfile.
    You can run as Docker container with these command:
    Docker-compose up 
    It's available on port 8080

## Project Directory
    
        componentes 
            reusable components are available in this directory
    
        constants  
            basic configuration and constants are available in this directory
    
        hooks   
            reusable stateful logic (named it hooks) are available in this directory
    
        routes
            routes and pages are available in this directory
    
        utils
            reusable base logic and utils are available here
        
        types
            global typescript type is here


## Future improvement points
    
    1. We can add cost per destination and get this cost from passengers. Mission can be created , if total of wealth will become destination cost.
    2. We can add discount in a destination in a specific time.
    3. We can merge two mission if they have same date and destination.
    