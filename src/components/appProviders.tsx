import React from "react";
import { createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { BrowserRouter } from "react-router-dom";

const AppProviders = ({ children }: { children: React.ReactNode }) => {
  const theme = createTheme({ palette: { mode: "light" } });

  return (
    <ThemeProvider {...{ theme }}>
      <BrowserRouter>{children}</BrowserRouter>
      <CssBaseline />
    </ThemeProvider>
  );
};

export default AppProviders;
