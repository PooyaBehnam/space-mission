import { Box } from "@mui/material";
import Container from "@mui/material/Container";

type LayoutProps = {
  children: React.ReactNode;
};

const Layout = ({ children }: LayoutProps) => {
  return (
    <Box
      sx={{
        minHeight: "calc(100vh)",
        background: "lightgray",
      }}
    >
      <Box component={"header"} sx={{ background: "white", p: 3 }}>
        Jounrney To Mars
      </Box>
      <Container component="main" maxWidth="lg" sx={{ py: 5 }}>
        {children}
      </Container>
    </Box>
  );
};

export default Layout;
