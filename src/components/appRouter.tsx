import { Navigate, useRoutes } from "react-router-dom";
import CreateMission from "../routes/createMission.page";
import EditMission from "../routes/editMission.page";
import MissionList from "../routes/missionList.page";

const AppRouter = () => {
  const appRoutes = useRoutes([
    { path: "/missions", element: <MissionList /> },
    { path: "/mission/create", element: <CreateMission /> },
    { path: "/mission/edit/:id", element: <EditMission /> },
    { path: "*", element: <Navigate to={"/missions"} /> },
  ]);

  return appRoutes;
};

export default AppRouter;
