import {
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from "@mui/material";
import React from "react";
import { DESTINATION_LIST } from "../constants";
import { MissionMainInfo } from "../types";

type MainInforFormType = {
  setMission: React.Dispatch<React.SetStateAction<MissionMainInfo>>;
  mission: MissionMainInfo;
};
const MainInfoForm = ({ setMission, mission }: MainInforFormType) => {
  const { name, destination, date } = mission;

  const handleChange = (
    e:
      | SelectChangeEvent<typeof DESTINATION_LIST | null>
      | React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;

    setMission({ ...mission, [name]: value });
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={3}>
        <TextField
          fullWidth
          name="name"
          label="name"
          InputLabelProps={{ shrink: true }}
          onChange={handleChange}
          value={name}
          error={!Boolean(name)}
          helperText={!Boolean(name) && "Required"}
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <FormControl fullWidth error={!Boolean(destination)}>
          <InputLabel id="destination-select-label" shrink={true}>
            destination
          </InputLabel>
          <Select
            labelId="destination-select-label"
            id="destination-simple-select"
            name="destination"
            value={destination}
            onChange={handleChange}
            error={!Boolean(destination)}
          >
            {DESTINATION_LIST.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          {!Boolean(destination) && <FormHelperText>Required</FormHelperText>}
        </FormControl>
      </Grid>
      <Grid item xs={12} md={3}>
        <TextField
          type={"date"}
          fullWidth
          id="date"
          name="date"
          label="date"
          value={date}
          InputLabelProps={{ shrink: true }}
          onChange={handleChange}
          error={!Boolean(date)}
          helperText={!Boolean(date) && "Required"}
        />
      </Grid>
    </Grid>
  );
};

export default MainInfoForm;
