import { IconButton, TableCell, TableRow, Typography } from "@mui/material";
import { MissionType } from "../types";
import EditIcon from "@mui/icons-material/Edit";
import { getDiffFromToDay } from "../utils/timeHelper";
import { Link } from "react-router-dom";

type MissionListItemType = { mission: MissionType };
const MissionListItem = ({ mission }: MissionListItemType) => {
  const { name, members, destination, date, id } = mission;
  const diffDay = getDiffFromToDay(date);
  const isDeparted = diffDay < 0;
  return (
    <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
      <TableCell align="center">{name}</TableCell>
      <TableCell align="center">{members.length}</TableCell>
      <TableCell align="center">{destination}</TableCell>
      <TableCell align="center">
        <Typography>{date.toString()}</Typography>
        <Typography
          color={isDeparted ? "red" : "gray"}
          variant="subtitle2"
          component="p"
        >
          {isDeparted
            ? "Departed"
            : diffDay === 0
            ? "Today"
            : `In ${diffDay} days`}
        </Typography>
      </TableCell>
      <TableCell align="center">
        <IconButton component={Link} to={`/mission/edit/${id}`}>
          <EditIcon />
        </IconButton>
      </TableCell>
    </TableRow>
  );
};

export default MissionListItem;
