import { Grid, TextField } from "@mui/material";
import React from "react";
import { PilotType } from "../types";

type PilotFormType = {
  changeField: (field: string, value: string) => void;
  toggleHasError: () => void;
  member: PilotType;
};

const PilotForm = ({ changeField, member, toggleHasError }: PilotFormType) => {
  const { experience, hasError: memberHasError } = member;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    changeField(name, value);
  };

  React.useEffect(() => {
    const hasError = !experience || experience < 10;
    if (hasError !== memberHasError) {
      toggleHasError();
    }
  }, [experience, memberHasError, toggleHasError]);

  const hasError = !experience || experience < 10;
  return (
    <>
      <Grid item xs={12} md={3}>
        <TextField
          fullWidth
          id="experience"
          name="experience"
          label="experience"
          value={experience}
          onChange={handleChange}
          InputLabelProps={{ shrink: true }}
          error={hasError}
          helperText={hasError && "Required and More than 10 yrs"}
        />
      </Grid>
      <Grid item xs={12} md={3}></Grid>
    </>
  );
};

export default PilotForm;
