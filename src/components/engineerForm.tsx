import {
  FormControl,
  FormHelperText,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  TextField,
} from "@mui/material";
import React from "react";
import { JOB_LIST } from "../constants";
import { EngineerType, JobType } from "../types";

type EngineerFormType = {
  changeField: (field: string, value: string) => void;
  member: EngineerType;
  toggleHasError: () => void;
};
const EngineerForm = ({
  changeField,
  member,
  toggleHasError,
}: EngineerFormType) => {
  const { experience, job, hasError } = member;

  const handleChange = (
    e:
      | SelectChangeEvent<JobType>
      | React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;
    changeField(name, value);
  };

  React.useEffect(() => {
    const hasErr = !job;
    if (hasErr !== hasError) {
      toggleHasError();
    }
  }, [hasError, job, toggleHasError]);

  return (
    <>
      <Grid item xs={12} md={3}>
        <TextField
          fullWidth
          id="experience"
          name="experience"
          label="experience"
          InputLabelProps={{ shrink: true }}
          value={experience}
          onChange={handleChange}
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <FormControl fullWidth error={!job}>
          <InputLabel id="job-select-label" shrink={true}>
            Job
          </InputLabel>
          <Select
            labelId="job-select-label"
            id="job-simple-select"
            name={"job"}
            value={job}
            onChange={handleChange}
            error={!job}
          >
            {JOB_LIST.map((item) => (
              <MenuItem key={item} value={item}>
                {item}
              </MenuItem>
            ))}
          </Select>
          {!job && <FormHelperText>Required</FormHelperText>}
        </FormControl>
      </Grid>
    </>
  );
};

export default EngineerForm;
