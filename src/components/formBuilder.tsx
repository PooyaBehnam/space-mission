import {
  Box,
  FormControl,
  Grid,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import React, { useCallback } from "react";
import { MEMBER_TYPES } from "../constants";
import { PayloadType } from "../hooks/useMember";
import { Member } from "../types";
import EngineerForm from "./engineerForm";
import PassengerForm from "./passengerForm";
import PilotForm from "./pilotForm";
import CloseIcon from "@mui/icons-material/Close";

type FormBuilderType = {
  member: Member;
  dispatch: React.Dispatch<PayloadType>;
};
const FormBuilder = ({ member, dispatch }: FormBuilderType) => {
  const { type, id } = member;

  const changeField = useCallback(
    (field: string, value: string) => {
      dispatch({ action: "CHANGE_FIELD", id, field, value });
    },
    [dispatch, id]
  );

  const removeItem = useCallback(() => {
    dispatch({ action: "REMOVE", id });
  }, [dispatch, id]);

  const toggleHasError = React.useCallback(() => {
    dispatch({ action: "TOGGLE_HAS_ERROR", id });
  }, [dispatch, id]);

  const renderForm = () => {
    switch (type) {
      case "Pilot":
        return <PilotForm {...{ changeField, member, toggleHasError }} />;
      case "Engineer":
        return <EngineerForm {...{ changeField, member, toggleHasError }} />;
      case "Passenger":
        return <PassengerForm {...{ changeField, member, toggleHasError }} />;
      default:
        return null;
    }
  };

  return (
    <Box sx={{ px: 1, py: 3, borderBottom: 1 }}>
      <Grid container spacing={3}>
        <Grid item xs={12} md={3}>
          <FormControl fullWidth>
            <InputLabel id="demo-simple-select-label">Type</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              name="type"
              value={type}
              onChange={(e) => {
                changeField(e.target.name, e.target.value);
              }}
            >
              {MEMBER_TYPES.map((item) => (
                <MenuItem key={item} value={item}>
                  {item}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        {renderForm()}
        <Grid
          item
          xs={12}
          md={3}
          display={"flex"}
          alignItems={"center"}
          justifyContent={"center"}
        >
          <IconButton onClick={removeItem}>
            <CloseIcon />
          </IconButton>
        </Grid>
      </Grid>
    </Box>
  );
};

export default FormBuilder;
