import { Grid, TextField } from "@mui/material";
import React from "react";

import { PassengerType } from "../types";
type PassengerFormType = {
  changeField: (field: string, value: string) => void;
  member: PassengerType;
  toggleHasError: () => void;
};
const PassengerForm = ({
  changeField,
  member,
  toggleHasError,
}: PassengerFormType) => {
  const { age, wealth, hasError: memberHasError } = member;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    changeField(name, value);
  };

  React.useEffect(() => {
    const hasError = !age;
    if (hasError !== memberHasError) {
      toggleHasError();
    }
  }, [age, memberHasError, toggleHasError]);

  return (
    <>
      <Grid item xs={12} md={3}>
        <TextField
          fullWidth
          type="number"
          id="age"
          name="age"
          label="age"
          onChange={handleChange}
          value={age}
          error={!age}
          helperText={!age && "Required"}
          InputLabelProps={{ shrink: true }}
        />
      </Grid>
      <Grid item xs={12} md={3}>
        <TextField
          fullWidth
          id="wealth"
          name="wealth"
          label="wealth"
          onChange={handleChange}
          value={wealth}
          InputLabelProps={{ shrink: true }}
        />
      </Grid>
    </>
  );
};

export default PassengerForm;
