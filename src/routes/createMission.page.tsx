import React from "react";
import { Alert, Box, Button, Paper, Typography } from "@mui/material";
import Layout from "../components/layout";
import { MissionMainInfo } from "../types";
import { v4 as uuidv4 } from "uuid";
import useMember from "../hooks/useMember";
import FormBuilder from "../components/formBuilder";
import MainInfoForm from "../components/mainInfoForm";
import { StorageAccessor } from "../utils/storageAccessor";
import { useNavigate } from "react-router-dom";
import { MISSION_STORAGE_KEY } from "../constants";

const CreateMission = () => {
  const navigate = useNavigate();
  const [mission, setMission] = React.useState<MissionMainInfo>({
    name: "",
    destination: null,
    date: "",
  } as unknown as MissionMainInfo);
  const { name, destination, date } = mission;
  const { members, dispatch, errorMsgs } = useMember();

  const addMore = () => {
    dispatch({ action: "ADD_MORE", type: "Pilot" });
  };

  const onSubmit = () => {
    const storageAccessor = new StorageAccessor(MISSION_STORAGE_KEY);

    storageAccessor.value = [
      ...(storageAccessor.value ? storageAccessor.value : []),
      { ...mission, members, id: uuidv4() },
    ];
    navigate("/missions");
  };
  const mainFormIsValid = !!name && !!destination && !!date && true;
  return (
    <Layout>
      <Box sx={{ display: "flex", justifyContent: "space-between", mb: 2 }}>
        <Typography component="h1" variant="h5">
          Configure a new mission
        </Typography>
      </Box>
      <Paper sx={{ p: 2 }}>
        <MainInfoForm {...{ mission, setMission }} />
        <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
          <Typography component="h1" variant="h5">
            Members
          </Typography>
        </Box>
        {members.map((member) => (
          <FormBuilder key={member.id} {...{ member, dispatch }} />
        ))}
        <Box
          sx={{
            px: 1,
            py: 3,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="contained" size="large" onClick={addMore}>
            New Member
          </Button>
        </Box>
      </Paper>
      <Box sx={{ px: 1, py: 3 }}>
        {errorMsgs.map((item) => (
          <Alert severity="error" key={item} sx={{ my: 2 }}>
            {item}
          </Alert>
        ))}
        <Button
          variant="contained"
          disabled={errorMsgs.length > 0 || !mainFormIsValid}
          onClick={onSubmit}
        >
          Create
        </Button>
      </Box>
    </Layout>
  );
};

export default CreateMission;
