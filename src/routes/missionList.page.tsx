import Layout from "../components/layout";
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { Clear as ClearIcon } from "@mui/icons-material";
import React from "react";
import { MissionType } from "../types";
import { StorageAccessor } from "../utils/storageAccessor";
import MissionListItem from "../components/missionListItem";
import { Link } from "react-router-dom";
import { MISSION_STORAGE_KEY } from "../constants";

const MissionList = () => {
  const [q, setQ] = React.useState<string>("");
  const [data, setData] = React.useState<MissionType[]>([]);

  React.useEffect(() => {
    const data = new StorageAccessor(MISSION_STORAGE_KEY);
    setData(data.value || []);
  }, []);

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { value },
    } = e;
    setQ(value.trim());
  };

  return (
    <Layout>
      <Box sx={{ display: "flex", justifyContent: "space-between", mb: 2 }}>
        <Typography component="h1" variant="h5">
          Missions
        </Typography>
        <Button variant="contained" component={Link} to={"/mission/create"}>
          New Mission
        </Button>
      </Box>
      <Box
        sx={{
          width: 500,
          maxWidth: "100%",
        }}
      ></Box>
      <TableContainer component={Paper} variant="outlined" sx={{ p: 3 }}>
        <FormControl sx={{ m: 1, width: "25ch" }} variant="outlined">
          <InputLabel htmlFor="search">Search</InputLabel>
          <OutlinedInput
            value={q}
            onChange={handleSearch}
            id="search"
            endAdornment={
              <InputAdornment position="end">
                <IconButton onClick={() => setQ("")} edge="end">
                  <ClearIcon />
                </IconButton>
              </InputAdornment>
            }
            label="search"
          />
        </FormControl>
        <Table size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Name</TableCell>
              <TableCell align="center">Members</TableCell>
              <TableCell align="center">Destination</TableCell>
              <TableCell align="center">Departure</TableCell>
              <TableCell align="center">Action</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .filter(({ name }) => name.includes(q))
              .map((mission) => (
                <MissionListItem key={mission.id} {...{ mission }} />
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Layout>
  );
};

export default MissionList;
