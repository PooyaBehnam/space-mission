import React from "react";
import { Alert, Box, Button, Paper, Typography } from "@mui/material";
import Layout from "../components/layout";
import { Member, MissionMainInfo } from "../types";

import useMember from "../hooks/useMember";
import FormBuilder from "../components/formBuilder";
import MainInfoForm from "../components/mainInfoForm";
import { StorageAccessor } from "../utils/storageAccessor";
import { useNavigate, useParams } from "react-router-dom";
import { MISSION_STORAGE_KEY } from "../constants";

const EditMission = () => {
  const navigate = useNavigate();
  const [mission, setMission] = React.useState<MissionMainInfo>({
    name: "",
    destination: null,
    date: "",
  } as unknown as MissionMainInfo);
  const params = useParams();
  const { name, destination, date } = mission;
  const { id: missionId } = params;
  const { members, dispatch, errorMsgs } = useMember();

  const addMore = () => {
    dispatch({ action: "ADD_MORE", type: "Pilot" });
  };

  React.useEffect(() => {
    const storageAccessor = new StorageAccessor(MISSION_STORAGE_KEY);
    const storage = storageAccessor.value || [];
    const data = storage.filter(
      (item: { id: string | undefined }) => item.id === missionId
    )[0];
    const { name, destination, date, members = [] } = data || {};

    setMission({ name, destination, date });
    dispatch({ action: "SET_MEMBERS", members });
  }, [dispatch, missionId]);

  const onSubmit = () => {
    const storageAccessor = new StorageAccessor(MISSION_STORAGE_KEY);
    const data = storageAccessor.value;

    storageAccessor.value = data.map((item: Member) =>
      item.id === missionId ? { id: missionId, members, ...mission } : item
    );
    navigate("/missions");
  };

  const mainFormIsValid = !!name && !!destination && !!date && true;
  return (
    <Layout>
      <Box sx={{ display: "flex", justifyContent: "space-between", mb: 2 }}>
        <Typography component="h1" variant="h5">
          Configure a new mission
        </Typography>
      </Box>
      <Paper sx={{ p: 2 }}>
        <MainInfoForm {...{ mission, setMission }} />
        <Box sx={{ display: "flex", justifyContent: "space-between", mt: 2 }}>
          <Typography component="h1" variant="h5">
            Members
          </Typography>
        </Box>
        {members.map((member) => (
          <FormBuilder key={member.id} {...{ member, dispatch }} />
        ))}
        <Box
          sx={{
            px: 1,
            py: 3,
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Button variant="contained" size="large" onClick={addMore}>
            New Member
          </Button>
        </Box>
      </Paper>
      <Box sx={{ px: 1, py: 3 }}>
        {errorMsgs.map((item) => (
          <Alert severity="error" key={item} sx={{ my: 2 }}>
            {item}
          </Alert>
        ))}
        <Button
          variant="contained"
          disabled={errorMsgs.length > 0 || !mainFormIsValid}
          onClick={onSubmit}
        >
          Update
        </Button>
      </Box>
    </Layout>
  );
};

export default EditMission;
