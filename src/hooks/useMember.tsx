import React from "react";
import { EngineerType, Member, memberType } from "../types";
import { v4 as uuidv4 } from "uuid";

export type ActionType =
  | "ADD_MORE"
  | "REMOVE"
  | "ADD_FIELD"
  | "CHANGE_FIELD"
  | "SET_MEMBERS";

export type PayloadType =
  | { action: "ADD_MORE"; type: memberType }
  | { action: "REMOVE" | "TOGGLE_HAS_ERROR"; id: string }
  | { action: "CHANGE_FIELD"; field: string; value: string; id: string }
  | { action: "SET_MEMBERS"; members: Member[] };

const reducer = (state: Member[], payload: PayloadType): Member[] => {
  switch (payload.action) {
    case "ADD_MORE":
      return [...state, { id: uuidv4(), type: payload.type } as Member];
    case "REMOVE":
      return state.filter((x) => x.id !== payload.id);
    case "CHANGE_FIELD":
      return state.map((el) =>
        el.id === payload.id ? { ...el, [payload.field]: payload.value } : el
      );
    case "TOGGLE_HAS_ERROR":
      return state.map((el) =>
        el.id === payload.id ? { ...el, hasError: !el.hasError } : el
      );
    case "SET_MEMBERS":
      return payload.members;

    default:
      return state;
  }
};

const useMember = (initialState: Member[] = []) => {
  const [members, dispatch] = React.useReducer(reducer, initialState);

  const hasDuplicateJob = () => {
    const selectedEngineers = members.filter(
      (item) => item.type === "Engineer" && item.job
    ) as EngineerType[];
    const jobs = selectedEngineers.map((x) => x.job);
    const noDups = new Set(jobs);

    return jobs.length !== noDups.size;
  };

  const hasOnlyOnePilot =
    members.filter((item) => item.type === "Pilot").length === 1;
  const hasMoreThanOnePassenger =
    members.filter((item) => item.type === "Passenger").length >= 1;
  const hasErrorInMemberForm = members.every((item) => !item.hasError);
  return {
    members,
    dispatch,
    hasDuplicateJob: hasDuplicateJob(),
    hasError:
      members.some((item) => !item.hasError) &&
      !hasOnlyOnePilot &&
      !hasMoreThanOnePassenger,
    errorMsgs: [
      ...(!hasOnlyOnePilot ? ["A mission must have exactly 1 pilot"] : []),
      ...(!hasMoreThanOnePassenger
        ? ["A Mission must have At least 1 passenger"]
        : []),
      ...(!hasErrorInMemberForm ? ["has Error in Members form"] : []),
      ...(hasDuplicateJob()
        ? [
            "All engineers have different job (can't have 2 engineers with the same job)",
          ]
        : []),
    ],
  };
};

export default useMember;
