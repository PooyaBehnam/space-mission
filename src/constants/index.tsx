import { JobType, memberType } from "../types";

export const JOB_LIST: JobType[] = [
  "Solar panels",
  "Maintenance",
  "Mechanics",
  "Navigation",
];

export const MEMBER_TYPES: memberType[] = ["Pilot", "Engineer", "Passenger"];

export const DESTINATION_LIST: string[] = [
  "Mars Alpha-110",
  "Mars Alpha-115",
  "Mars Alpha-120",
  "Mars Alpha-125",
  "Mars Alpha-130",
  "Mars Alpha-135",
];

export const MISSION_STORAGE_KEY = "MISSION_DATA"