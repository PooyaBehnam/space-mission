import "./App.css";
import AppProviders from "./components/appProviders";
import AppRouter from "./components/appRouter";

function App() {
  return (
    <AppProviders>
      <AppRouter />
    </AppProviders>
  );
}

export default App;
