export class StorageAccessor {
  private key: string;

  constructor(key: string) {
    this.key = key;
  }

  set value(value: any) {
    localStorage.setItem(this.key, JSON.stringify(value));
  }

  get value() {
    const value = localStorage.getItem(this.key);
    if (!value) {
      return null;
    }
    return JSON.parse(value);
  }

  reset() {
    localStorage.removeItem(this.key);
  }
}
