export const getDiffFromToDay = (date: string) =>
  Math.ceil(
    (new Date(date).getTime() - new Date().getTime()) / (1000 * 3600 * 24)
  );
