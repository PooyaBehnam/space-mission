import { DESTINATION_LIST } from "../constants";

export type JobType =
  | "Navigation"
  | "Solar panels"
  | "Maintenance"
  | "Mechanics"
  | "Navigation";

export type memberType = "Pilot" | "Engineer" | "Passenger";

export type PilotType = {
  type: "Pilot";
  experience: number;
  id: string;
  hasError: boolean;
};

export type EngineerType = {
  type: "Engineer";
  experience: number;
  job: JobType;
  id: string;
  hasError: boolean;
};

export type PassengerType = {
  type: "Passenger";
  age: number;
  wealth: number;
  id: string;
  hasError: boolean;
};

export type Member = PilotType | EngineerType | PassengerType;

export type MissionMainInfo = {
  id?:string,
  name: string;
  destination: typeof DESTINATION_LIST | undefined;
  date: string;
}
export type MissionType = {
  members: Member[];
} & MissionMainInfo;
